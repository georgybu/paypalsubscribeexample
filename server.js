var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var server = require('http').Server(app);
app.engine('html', require('ejs').renderFile);
app.set('views', __dirname);
app.set('view engine', 'html');
app.use(bodyParser.json());
app.use('/', express.static(__dirname + '/'));

app.get('/', function (req, res) {
  res.render('index');
});
app.get('/return', function (req, res) {
  res.render('return');
});
app.get('/cancel', function (req, res) {
  res.render('cancel');
});

app.get('/ipn', function (req, res) {
  // this is paypal IPN
  console.dir(req);
  res.render('ipn');
});
app.post('/ipn', function (req, res) {
  // this is paypal IPN
  console.dir(req);
  res.render('ipn');
});

app.set('port', 3000);
server.listen(app.get('port'), function () {
  console.log('server listening on port ' + server.address().port);
});
